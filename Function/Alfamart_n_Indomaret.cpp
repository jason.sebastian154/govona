void topupalfamart () {
	char input;
	//inputclearscreenhere
	puts("===========================================================\n");
    puts("   <> <> <> == HOW TO TOP UP THROUGH ALFAMART == <> <> <>  \n");
    puts("===========================================================\n");
	printf ("1. Come to the nearest Alfamart\n");
	printf ("2. Tell the cashier that you want to Top Up for GoVoNa\n");
	printf ("3. Tell them GoVoNa's product code 696 + your number\n");
	printf ("4. Example : 6960818207839\n");
	printf ("5. The cashier will confirm your code by asking you your name\n");
	printf ("6. If it match the data from our server then you can move on to the next step\n");
	printf ("7. Tell the cashier how much you wanted to Top Up\n");
	printf ("8. Pay the cashier with Top Up + Rp.2000 for administration fee\n");
	printf ("9. Lastly the cashier will confirm your payment and will give you the Top Up receipt\n");
	printf ("10. Save your receipt and confirm your Top Up have been accepted by checking GoVoNa app\n");
	puts (" ");
	printf ("[9] Back to Top Up Menu\n");
	printf ("[0] Main Menu\n");
    do 
    {
        printf("\nInput a number: "); scanf("%c",&input);
        getchar();
        switch (input) 
        {
            case '9': 
                topupmenu();
                break;
            case '0':
                mainMenu();
                break;
            default:
                printf("Input is not recognized.\n");
                break;
        }
    } while (input != '9' || input != '0');
}
//////////////////////////////////////////////////////////////////////////////////////////////
void topupindomaret () {
	char input;
	//inputclearscreenhere
	puts("===========================================================\n");
    puts("   <> <> <> == HOW TO TOP UP THROUGH INDOMARET == <> <> <>  \n");
    puts("===========================================================\n");
	printf ("1. Come to the nearest Indomaret\n");
	printf ("2. Tell the cashier that you want to Top Up for GoVoNa\n");
	printf ("3. Tell them GoVoNa's product code 696 + your number\n");
	printf ("4. Example : 6960818207839\n");
	printf ("5. The cashier will confirm your code by asking you your name\n");
	printf ("6. If it match the data from our server then you can move on to the next step\n");
	printf ("7. Tell the cashier how much you wanted to Top Up\n");
	printf ("8. Pay the cashier with Top Up + Rp.2000 for administration fee\n");
	printf ("9. Lastly the cashier will confirm your payment and will give you the Top Up receipt\n");
	printf ("10. Save your receipt and confirm your Top Up have been accepted by checking GoVoNa app\n");
	puts (" ");
	printf ("[9] Back to Top Up Menu\n");
	printf ("[0] Main Menu\n");
    do 
    {
        printf("\nInput a number: "); scanf("%c",&input);
        getchar();
        switch (input) 
        {
            case '9': 
                topupmenu();
                break;
            case '0':
                mainMenu();
                break;
            default:
                printf("Input is not recognized.\n");
                break;
        }
    } while (input != '9' || input != '0');
}
