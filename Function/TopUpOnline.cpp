#include <stdio.h>

void topUpOnline();
void topUpMBanking();
void topUpAtm();
void clrscr();
void bcaMobile() {
    puts("===================================================================");
    puts(" <> <> <> == HOW TO TOP UP THROUGH BCA MOBILE BANKING == <> <> <>  ");
    puts("===================================================================\n");
    printf ("\n");
    printf ("1. Buka Aplikasi BCA Mobile, Lalu pilih M-BCA\n");
    printf ("2. Masukkan kode akses untuk login\n");
    printf ("3. Pilih menu m-transfer\n");
    printf ("4. Pilih BCA Virtual Account \n");
    printf ("5. Masukkan nomor virtual account BCA GoVoNa, yaitu 12345 + no HP yang terdaftar di GoVoNa, lalu pilih 'OK'\n");
    printf ("6. Setelah itu, pilih tombol Send, jika nomor benar maka akan muncul nama pemilik akun GoVoNa, lanjut memilik 'OK'\n");
    printf ("7. Masukkan nominal saldo yang hendak di-top up\n");
    printf ("8. Masukkan pin M-BCA, lalu pilih �OK�\n");
    printf ("9. Jika transfer berhasil, makan akan muncul pemberitahuan dan saldo pada akun yang di-top up sehingga saldo akan \n");
	printf ("   langsung bertambah.\n");
    
    getchar ();
    clrscr();
    topUpMBanking();
    
}

void mandiriMobile(){
	puts("=======================================================================");
    puts(" <> <> <> == HOW TO TOP UP THROUGH MANDIRI MOBILE BANKING == <> <> <>  ");
    puts("=======================================================================\n");
    printf ("\n");
    printf ("1. Buka aplikasi Mandiri Online\n");
    printf ("2. Login ke akun Mandiri Online Anda\n");
    printf ("3. Pilih menu Pembayaran\n");
    printf ("4. Pilih menu Buat Pembayaran Baru \n");
    printf ("5. Pilih menu Multipayment\n");
    printf ("6. Pilih GoVoNa dan masukkan nomor ponsel Anda: 0812-xxxx-xxxx\n");
    printf ("7. Masukkan nominal top-up GoVoNa yang anda inginkan\n");
    printf ("8. Ikuti instruksi untuk menyelesaikan transaksi\n");
    
    getchar ();
    clrscr();
    topUpMBanking();
}

void briMobile(){
	puts("===================================================================");
    puts(" <> <> <> == HOW TO TOP UP THROUGH BRI MOBILE BANKING == <> <> <>  ");
    puts("===================================================================\n");
    printf ("\n");
    printf ("1. Masuk ke dalam akun BRI Mobile Anda dengan memasukkan informasi yang diminta.\n");
    printf ("2. Begitu Anda berhasil masuk ke dalam mobile banking BRI, layar akan menampilkan sederet menu. Pilih menu BRIVIA\n");
    printf ("3. Masukkan kode nomor virtual account GoVona BRI yang diawali angka 12345 + nomor HP Anda yang terdaftar di akun GoVoNa\n");
    printf ("4. nominal saldo yang ingin Anda tambahkan ke dalam akun GoVona Anda. \n");
    printf ("5. Ikuti petunjuk dan langkah-langkah berikut yang diinstruksikan untuk melanjutkan dan menyelesaikan transaksi Anda.\n");
    
    getchar();
    clrscr();
    topUpMBanking();
	
}

void bcaAtm(){
	puts("========================================================");
    puts(" <> <> <> == HOW TO TOP UP THROUGH BCA ATM == <> <> <>  ");
    puts("========================================================\n");
    printf ("\n");
    printf ("1. Masukkan kartu ATM BCA ke mesin ATM, lalu masukkan pin\n");
    printf ("2. Pilih menu 'transaksi lainnya'\n");
    printf ("3. Lalu, klik 'transfer'\n");
    printf ("4. Pilih 'Ke Rek BCA Virtual Account' \n");
    printf ("5. Masukkan nomor virtual account GoVoNa yang akan diisi saldo, yakni kode perusahaan GoVoNa 12345+no HP terdaftar\n");
	printf ("   lalu pilih  'Benar'\n");
    printf ("6. Ketika muncul nomor virtual GoVoNa, nama akun, dan nama produk, silakan isi jumlah top up (minimal Rp20.000)\n");
	printf ("   lalu klik 'Benar'\n");
    printf ("7. Pada layar konfirmasi akan muncul pertanyaan mengenai apakah data yang dimasukkan sudah tepat, jika tepat\n");
	printf ("   pilih 'Iya'\n");
    printf ("8. Tunggu hingga transaksi selesai dan struk pengisian saldo GoVoNa dicetak keluar dari mesin ATM sebagai bukti\n");
	printf ("   pengisian ulang\n");
    
    getchar ();
    clrscr();
    topUpAtm();
	
}

void mandiriAtm(){
	puts("============================================================");
    puts(" <> <> <> == HOW TO TOP UP THROUGH MANDIRI ATM == <> <> <>  ");
    puts("============================================================\n");
    printf ("\n");
    printf ("1. Masukkan kartu ATM dan PIN Mandiri Anda\n");
    printf ("2. Pilih menu Bayar/Beli\n");
    printf ("3. Pilih menu Lainnya, pilih lagi menu Lainnya\n");
    printf ("4. Pilih menu E-Commerce \n");
    printf ("5. Masukkan kode GoVoNa 12345, Khusus bank Mandiri\n");
	printf ("   Nomor Khususnya yakni 12345 + Nomor Ponsel Anda. Misal 0812-XXXX-XXXX\n");
    printf ("6. Masukkan nominal top-up yang anda inginkan\n");
    printf ("7. Ikuti instruksi untuk menyelesaikan transaksi\n");
    
    getchar ();	
    clrscr();
    topUpAtm();
}

void briAtm(){
	puts("========================================================");
    puts(" <> <> <> == HOW TO TOP UP THROUGH BRI ATM == <> <> <>  ");
    puts("========================================================\n");
    printf ("\n");
    printf ("1. Masukkan kartu ATM dan PIN BRI Anda\n");
    printf ("2. Pilih menu Transaksi Lain lalu pilih Bank Lain\n");
    printf ("3. Pilih menu Pembayaran lalu pilih Bank Lain\n");
    printf ("4. Pilih menu Lainnya lalu pilih Bank Lain\n");
    printf ("5. Pilih BRIVA\n");
    printf ("6. Masukkan 12345 + nomor ponsel Anda 12345+ 08xx-xxxx-xxxx\n");
    printf ("7. Masukkan nominal transfer\n");
    printf ("8. Ikuti instruksi untuk menyelesaikan transaksi\n");
    
    getchar();
    clrscr();
    topUpAtm();
	
}

void clrscr(){
	
	for (int i = 0; i <= 40; i++){
    printf("\n");
	}
	
}

void topUpMBanking(){
	int input = 0;
	puts("===================================================");
    puts(" <> <> <> == TOP UP Melalui M-Banking == <> <> <>  ");
    puts("===================================================\n");
    printf("Pilih Salah Satu Menu! \n");
    printf("1. Top Up Via BCA\n");
    printf("2. Top Up Via Mandiri\n");
    printf("3. Top Up Via BRI\n");
    printf("4. Back to Top Up Online Menu\n\n");
	do{
		printf(">> "); scanf("%d",&input);
		getchar();
		switch(input){
		
		case 1:
			clrscr();
			bcaMobile();
		case 2:
			clrscr();
			mandiriMobile();
		case 3:
			clrscr();
			briMobile();
		case 4:
			clrscr();
			topUpOnline();
		}
		
	}while (input<1 || input > 4);	
	
	
	
}

void topUpAtm(){
	int input = 0;
	puts("=============================================");
    puts(" <> <> <> == TOP UP Melalui ATM == <> <> <>  ");
    puts("=============================================\n");
    printf("Pilih Salah Satu Menu! \n");
    printf("1. Top Up Via BCA\n");
    printf("2. Top Up Via Mandiri\n");
    printf("3. Top Up Via BRI\n");
    printf("4. Back to Top Up Online Menu\n\n");
	do{
		printf(">> "); scanf("%d",&input);
		getchar();
		switch(input){
		
		case 1:
			clrscr();
			bcaAtm();
		case 2:
			clrscr();
			mandiriAtm();
		case 3:
			clrscr();
			briAtm();
		case 4:
			clrscr();
			topUpOnline();
		}
		
	}while (input<1 || input > 4);	
}

void topUpOnline(){
	int input = 0;
	puts("========================================");
    puts(" <> <> <> == TOP UP ONLINE == <> <> <>  ");
    puts("========================================\n");
    printf("Pilih Salah Satu Menu! \n");
    printf("1. Top Up Via M-Banking\n");
    printf("2. Top Up Via ATM\n");
    printf("3. Back to Top Up Menu\n");
    printf("4. Back to Main Menu\n\n")
	do{
		printf(">> "); scanf("%d",&input);
		getchar();
		switch(input){
		
		case 1:
			clrscr();
			topUpMBanking();
		case 2:
			clrscr();
			topUpAtm();
		case 3:
			clrscr();
			topupmenu();
		case 4:
			clrscr();
			MainMenu();
			// main menu top up
		;
		}
		
	}while (input<1 || input > 3);	
	
}

int main(){
	topUpOnline();
	
	return 0;
}
