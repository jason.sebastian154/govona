#include <stdio.h>

void mainSettings (){
	int inputAngka;
	puts("============================================\n");
	puts(" <> <> <>	    GoVoNa 	     <><><>	\n");
	puts(" <> <> <>	   Settings 	     <><><>	\n");
	puts("============================================\n");
	puts("[1] Profile\n"); // pergi ke menu profile
	puts("[2] Log Out\n"); // kembali ke menu log in awal
	puts("[0] Back To Main Menu\n"); // kembali ke main menu
	puts("============================================\n");
	do {
		printf("Input a number: ");
		scanf("%d", &inputAngka);
		getchar();
		switch (inputAngka){
			case 1:
				// profile();
				break;
			case 2:
				// logIn();
				break;
			case 0:
				mainMenu();
				break;
			default:
				printf("Input is not recognized!\n");
				break;
			} 
		} while (inputAngka < 0 || inputAngka > 2);
	}

int main(){
	mainSettings();
	return 0;
}
