void topupmenu() {
    char inputmenu;
    //inputclearscreenhere
    puts("===========================================================\n");
    puts("       <> <> <>      == TOP UP MENU ==      <> <> <>       \n");
    puts("===========================================================\n");
    printf("[1] Top Up Online\n");
    printf("[2] Top Up Offline\n");
    printf("[0] Back to Main Menu\n");
    printf("\n");
    puts("===========================================================\n");
    do 
    {
        printf("\nInput a number: "); scanf("%c",&inputmenu);
        getchar();
        switch (inputmenu) 
        {
            case '1': 
                topuponline();
                break;
            case '2':
                topupoffline();
                break;
            case '0':
                mainMenu();
                break;
            default:
                printf("Input is not recognized.\n");
                break;
        }
    } while (inputmenu < '0' || inputmenu > '2');
}


void topupminioffline() {
    char inputmenu;
    //inputclearscreenhere
    puts("===========================================================\n");
    puts("       <> <> <>      == TOP UP OFFLINE ==      <> <> <>     n");
    puts("===========================================================\n");
    printf("[1] ALFAMART\n");
    printf("[2] INDOMARET\n");
    printf("[0] Back to Main Menu\n");
    printf("\n");
    puts("===========================================================\n");
    do 
    {
        printf("\nInput a number: "); scanf("%c",&inputmenu);
        getchar();
        switch (inputmenu) 
        {
            case '1': 
                topupalfamart();
                break;
            case '2':
                topupindomaret();
                break;
            case '0':
                mainMenu();
                break;
            default:
                printf("Input is not recognized.\n");
                break;
        }
    } while (inputmenu < '0' || inputmenu > '2');
}

